NAME = filler
SRC = srcs/main.c srcs/map_functions.o srcs/calculate1.c srcs/place.c srcs/calculate2.c
OBJ = main.o map_functions.o calculate1.o place.o calculate2.o
INC = -I./includes
LIB = -L./libft -lft
SRCS = srcs/main.c srcs/map_functions.c srcs/calculate1.c

all: $(OBJ)
	cc $(OBJ) $(LIB) -Wall -Wextra -Werror

$(OBJ): $(SRCS)
	gcc -c $(SRCS) -Wall -Wextra -Werror

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)

re:
	@make fclean && make
