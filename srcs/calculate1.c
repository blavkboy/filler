/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate1.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/14 14:18:07 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/22 18:12:56 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

/*
** get_map
** purpose:				This function quite literally gets the map from the standard output.
**						It first checks the coordinates and puts them into the dim field in
**						the s_game struct. The function then reads the map from the stdout.
**						All this data is fed into the game struct.
*/

void		get_map(t_game *game)
{
	char	*line;
	size_t	y;

	y = 0;
	line = NULL;
	get_next_line(0, &line);
	read_dimensions(&line, game);
	get_next_line(0, &line);
	ft_strdel(&line);
	game->map = ft_memalloc(sizeof(char*) * game->dim[0]);
	while (y < game->dim[0])
	{
		get_next_line(0, &line);
		game->map[y] = ft_strsub(line, 4, game->dim[1]);
		ft_strdel(&line);
		y++;
	}
}

/*
** read_dimensions
** purpose:				This function reads the dimensions of the map from the line variable.
*/

void		read_dimensions(char **r_line, t_game *game)
{
	size_t	x;
	size_t	y;
	char	*line;

	line = *r_line;
	x = 0;
	y = 0;
	while (line[x])
	{
		if (ft_isdigit(line[x]))
		{
			game->dim[y] = ft_atoi(&line[x]);
			x++;
			y++;
		}
		if (y > 1)
			break ;
		x++;
	}
	ft_strdel(r_line);
}

/*
** get_token
** purpose:				This function works exatly the same way as the get_map function but it reads
**						and copies the game piece to store it in the token field of the s_game struct.
**						
*/

void		get_token(t_game *game)
{
	char	*line;
	size_t	i;

	i = 0;
	line = 0;
	game->token = ft_memalloc(sizeof(char**) * game->token_dim[0]);
	while (i < game->token_dim[0])
	{
		get_next_line(0, &line);
		game->token[i] = ft_strsub(line, 0, ft_strlen(line));
		ft_strdel(&line);
		i++;
	}
}

/*
** read_token_dim
** purpose:				This function reads the dimensions of the token and passes them to the s_game
**						struct.
*/

void		read_token_dim(char **r_line, t_game *game)
{
	size_t	x;
	size_t	y;
	char	*line;

	line = *r_line;
	x = 0;
	y = 0;
	while (line[x])
	{
		if (ft_isdigit(line[x]))
		{
			game->token_dim[y] = ft_atoi(&line[x]);
			x++;
			y++;
		}
		if (y > 1)
			break ;
		x++;
	}
	ft_strdel(r_line);
}

/*
** determine_player
** purpose:				This function determines the player you are whether that is first or second, this way
**						you know what your player character is between 'X' and 'O' respectively.
*/

void		determine_player(char **line, t_game *game)
{
	char	*ptr;

	ptr = ft_strchr(*line, 'p');
	ptr++;
	game->p = ft_atoi(ptr);
	ft_strdel(line);
	if (game->p == 1)
		game->piece = 'O';
	else if (game->p == 2)
		game->piece = 'X';
}
