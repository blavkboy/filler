/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   place.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 10:19:42 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/23 09:38:35 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

static int		ft_enough_space(t_game *game, size_t x, size_t y)
{
	if (x + game->token_dim[0] > game->dim[0])
		return (1);
	if (y + game->token_dim[1] > game->dim[1])
		return (1);
	return (0);
}

/*
** check_validity
** purpose:			This function checks if the place you want to place the piece
**					in is valid and it will return 1 if the place you wish to place
**					it in is valid. The biggest issue is dealing with the offset
**					from playing with bigger pieces. This function should also take
**					that into considertion first.
*/

static int		check_validity(t_game *game, size_t x, size_t y)
{
	size_t		size;
	size_t		c;
	size_t		i;
	size_t		w;
	char		opp;

	c = 0;
	i = 0;
	if (game->piece == 'O')
		opp = 'X';
	else
		opp = 'O';
	if (ft_enough_space(game, x, y))
		return (0);
	while (i < game->token_dim[0])
	{
		w = 0;
		while (w < game->token_dim[1])
		{
			if ((game->map[y + i][x + w] == game->piece) && 
					game->token[i][w] == '*')
				c++;
			if (game->map[y + i][x + w] == opp)
				c += 2;
			w++;
		}
		i++;
	}
	if (c == 1)
		return (1);
	return (0);
}

static void		ft_sendplay(t_game *game, size_t x, size_t y)
{
	t_moves		*moves;
	
	if (game->moves)
	{
		moves = game->moves;
		while (moves->next != NULL)
			moves = moves->next;
		moves->next = next_move(x, y);
	}
	else
	{
		game->moves = next_move(x, y);
	}
}

/*
** play_piece
** purpose:		Finds the first valid spot to place the piece and sends a message
**				to the stdout of the position it wants to play. In the case that
**				it can't find a valid spot it will send "0 0" to the stdout.
*/

void			place_piece(t_game *game)
{
	size_t	x;
	size_t	y;
	size_t	played;

	y = 0;
	played = 0;
	while (y + game->token_dim[0] - (game->offset[3] + game->offset[1]) <= game->dim[0])
	{
		x = 0;
		while (x + game->token_dim[1] - (game->offset[0] + game->offset[2]) <= game->dim[1])
		{
			if (check_validity(game, x, y))
			{
				ft_sendplay(game, x, y);
				played = 1;
			}
			x++;
		}
		y++;
	}
	if (!played)
		ft_putendl("");
	else
		pick_play(game);
}
