#include "../includes/player.h"

void        pick_play(t_game *game)
{
    int     play[2];
    t_moves *moves;

    moves = game->moves;
    ft_bzero(play, sizeof(int) * 2);
    play[0] = moves->play[0];
    play[1] = moves->play[1];
    moves = moves->next;
    while (moves)
    {
        if (moves->play[0] >= play[0])
            if (moves->play[1] < play[1])
            {
                play[0] = moves->play[0];
                play[1] = moves->play[1];
            }
        moves = moves->next;
    }
    ft_putcoord(play[0], play[1]);
}

void        ft_putcoord(size_t y, size_t x)
{
    ft_putnbr(y);
    ft_putchar(' ');
    ft_putnbr(x);
    ft_putchar('\n');
}