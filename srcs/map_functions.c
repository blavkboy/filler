/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_functions.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 11:26:26 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/22 18:13:58 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"


/*
** read_map
** purpose: 	starts by collecting the map dimensions so it can know how to read
**				from the file descriptor. It determins the player since that line
**				only comes once the first time the program prints that information
**				to the standard output. The next call is to get_map(), get_piece()
**				and play_piece() which are pretty self explanetory in their names.
*/
void		read_map(t_game *game)
{
	char	*line;

	line = NULL;
	game = ft_memalloc(sizeof(t_game));
	game->on = 1;
	get_next_line(0, &line);
	determine_player(&line, game);
	while (1)
	{
		get_map(game);
		get_piece(game);
		play_piece(game);
	}
}

/*
** get_piece
** purpose:		This function gets the game piece you are meant to play in your response.
**				The idea is to get the dimensions of the token(game piece) using the
**				function read_token_dim() and then it will know how much space to allocate
**				for the token in the function get_token(). All of this information is fed
**				into the t_game struct.
*/

void		get_piece(t_game *game)
{
	char	*line;

	line = NULL;
	get_next_line(0, &line);
	read_token_dim(&line, game);
	get_token(game);
	calculate_offset(game);
}

/*
** play_piece
** purpose:		This function first checks where it can place a piece on the map by checking if
**				one of two conditions is true. The first being if the piece is has only one
**				one of your characters overlapping it and if it does not overlap the other 
**				players pieces. We iterate through the entire map and make sure that our function
**				checks block by block for a safe spot to place a piece and return those coordinates
**				to the standard output formatted in the following fashion `[x y]`.
**/

void		play_piece(t_game *game)
{
	place_piece(game);
}
