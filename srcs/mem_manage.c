/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mem_manage.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 14:42:33 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/26 14:42:50 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

void		destroy_moves(t_game *game)
{
	t_moves	*moves;
	t_moves	*prev;

	moves = game->moves;
	game->moves = NULL;
	while (moves)
	{
		prev = moves;
		moves = moves->next;
		ft_memdel((void**)&prev->play);
		ft_memdel((void**)&prev);
	}
}

void		empty_map(t_game *game)
{
	size_t	itr;

	itr = 0;
	while (itr < game->dim[0])
	{
		ft_strdel(&game->map[itr]);
		itr++;
	}
	free(game->map);
	game->map = NULL;
	itr = 0;
	while (itr < game->token_dim[0])
	{
		ft_strdel(&game->token[itr]);
		itr++;
	}
	free(game->token);
	game->token = NULL;
}
