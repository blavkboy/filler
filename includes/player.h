/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 13:02:59 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/22 11:11:54 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER_H
# define PLAYER_H

# include "../libft/libft.h"
# include <fcntl.h>

typedef struct		s_moves
{
	size_t			*play;
	struct s_moves	*next;
}					t_moves;
typedef struct		s_game
{
	unsigned		on 	:1;
	unsigned		p	:2;
	size_t			dim[2];
	size_t			token_dim[2];
	char			piece;
	char			**map;
	char			**token;
	t_moves			*moves;
	size_t			offset[4];
}					t_game;
void				determine_player(char **line, t_game *game);
void				read_token_dim(char **r_line, t_game *game);
void				get_token(t_game *game);
void				get_map(t_game *game);
void				read_dimensions(char **r_line, t_game *game);
void				read_map(t_game *game);
void				get_piece(t_game *game);
void				play_piece(t_game *game);
void				place_piece(t_game *game);
void				calculate_offset(t_game *game);

# endif
