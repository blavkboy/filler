/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   place.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 10:19:42 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/21 13:20:35 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

static int		ft_enough_space(t_game *game, size_t x, size_t y)
{
	if (x + game->token_dim[0] > game->dim[0])
		return (1);
	if (y + game->token_dim[1] > game->dim[1])
		return (1);
	return (0);
}

/*
** check_validity
** purpose:			This function checks if the place you want to place the piece
**					in is valid and it will return 1 if the place you wish to place
**					it in is valid. The biggest issue is dealing with the offset
**					from playing with bigger pieces. This function should also take
**					that into considertion first.
*/

static int		check_validity(t_game *game, size_t x, size_t y)
{
	char		*mini_map;
	size_t		size;
	size_t		i;
	size_t		z;
	size_t		c;

	c = 0;
	z = 0;
	if (ft_enough_space(game, x, y))
		return (0);
	while (z < game->token_dim[1])
	{
		i = 0;
		while (i < game->token_dim[0])
		{
			if (game->token[y + z][y + x] == game->piece)
				c++;
			x++;
		}
		z++;
	}
	if (c == 1)
		return (1);
	return (0);
}

static void		ft_sendplay(size_t x, size_t y)
{
	ft_putnbr(y);
	ft_putstr(" ");
	ft_putnbr(x);
	ft_putendl("");
}

/*
** play_piece
** purpose:		Finds the first valid spot to place the piece and sends a message
**				to the stdout of the position it wants to play. In the case that
**				it can't find a valid spot it will send "-1 -1" to the stdout.
*/

void			place_piece(t_game *game)
{
	size_t	x;
	size_t	y;
	size_t	played;

	y = 0;
	played = 0;
	while (y < game->dim[0])
	{
		x = 0;
		while (x < game->dim[1])
		{
			if (check_validity(game, x, y))
			{
				ft_sendplay(x, y);
				played = 1;
			}
			x++;
		}
		y++;
	}
}
