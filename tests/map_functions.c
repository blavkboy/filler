/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_functions.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/10 11:26:26 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/14 16:12:14 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"

typedef struct		s_game
{
	unsigned		on 	:1;
	unsigned		p	:2;
	size_t			dim[2];
	size_t			token_dim[2];
	char			**map;
	char			**token;
	size_t			offset[4];
}					t_game;

void		read_map(void)
{
	t_game	*game;
	char	*line;

	game = ft_memalloc(sizeof(t_game));
	game->on = 1;
	get_next_line(0, &line);
	determine_player(&line, game);
	get_map(game);
	get_token(game);
}

void		get_game(t_game *game)
{
	size_t	x;
	size_t	y;
	char	*line;

	x = 0;
	y = 0;
	line = NULL;
	get_next_line(0, &line);
	read_token_dim(&line, game, &x, &y);
}

int         main(int ac, char **av)
{
    int     fd;
    char    *line;

    if (ac != 2)
        return (1);
}