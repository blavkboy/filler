/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 09:12:52 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/26 08:37:44 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include <fcntl.h>
#include <stdio.h>

typedef struct		s_moves
{
	size_t			*play;
	struct s_moves	*next;
}					t_moves;

typedef struct		s_game
{
	unsigned		on 	:1;
	unsigned		p	:2;
	size_t			dim[2];
	size_t			token_dim[2];
	char			piece;
	char			**map;
	char			**token;
	t_moves			*moves;
	size_t			offset[4];
}					t_game;

static void	bottom_offset(t_game *game)
{
	size_t	x;
	int		y;
	int		end;
	size_t	off;

	y = (int)game->token_dim[0] - 1;
	off = 0;
	end = 0;
	while (y >= 0 && !end)
	{
		x = 0;
		while (x < game->token_dim[1] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			x++;
		}
		if (end)
			break ;
		off++;
		y--;
	}
	game->offset[3] = off;
}

static void	right_offset(t_game *game)
{
	int		x;
	size_t	y;
	int		end;
	size_t off;

	off = 0;
	x = game->token_dim[1] - 1;
	end = 0;
	while (x >= 0&& !end)
	{
		y = 0;
		while (y < game->token_dim[0] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			y++;
		}
		if (end)
			break ;
		off++;
		x--;
	}
	game->offset[2] = off;
}

static void	top_offset(t_game *game)
{
	size_t	x;
	size_t	y;
	int		end;

	y = 0;
	end = 0;
	while (y < game->token_dim[0] && !end)
	{
		x = 0;
		while (x < game->token_dim[1] && !end)
		{
			if (game->token[y][x] == '*')
				end  = 1;
			x++;
		}
		if (end)
			break ;
		y++;
	}
	game->offset[1] = y;
}

static void	left_offset(t_game *game)
{
	size_t	x;
	size_t	y;
	int		end;

	end = 0;
	x = 0;
	while (x < game->token_dim[1] && !end)
	{
		y = 0;
		while (y < game->token_dim[0] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			y++;
		}
		if (end)
			break ;
		x++;
	}
	game->offset[0] = x;
}

void		calculate_offset(t_game *game)
{
	left_offset(game);
	right_offset(game);
	top_offset(game);
	bottom_offset(game);
}

void		read_dimensions(char **r_line, t_game *game)
{
	size_t	x;
	size_t	y;
	char	*line;

	line = *r_line;
	x = 0;
	y = 0;
	while (line[x])
	{
		if (ft_isdigit(line[x]))
		{
			game->dim[y] = ft_atoi(&line[x]);
			x++;
			y++;
		}
		if (y > 1)
			break ;
		x++;
	}
	ft_strdel(r_line);
}

void		get_map(t_game *game, int fd)
{
	char	*line;
	size_t	y;

	y = 0;
	line = NULL;
	get_next_line(fd, &line);
	read_dimensions(&line, game);
	get_next_line(fd, &line);
	ft_strdel(&line);
	game->map = ft_memalloc(sizeof(char*) * game->dim[0]);
	game->moves = NULL;
	while (y < game->dim[0])
	{
		get_next_line(fd, &line);
		game->map[y] = ft_strsub(line, 4, game->dim[1]);
		ft_strdel(&line);
		y++;
	}
}

void		determine_player(char **line, t_game *game)
{
	char	*ptr;

	ptr = ft_strchr(*line, 'p');
	ptr++;
	game->p = ft_atoi(ptr);
	ft_strdel(line);
	if (game->p == 1)
		game->piece = 'O';
	else
		game->piece = 'X';
}

void		read_token_dim(char **r_line, t_game *game)
{
	size_t	x;
	size_t	y;
	char	*line;

	line = *r_line;
	x = 0;
	y = 0;
	while (line[x])
	{
		if (ft_isdigit(line[x]))
		{
			game->token_dim[y] = ft_atoi(&line[x]);
			x++;
			y++;
		}
		if (y > 1)
			break ;
		x++;
	}
	ft_strdel(r_line);
}

void		get_token(t_game *game, int fd)
{
	char	*line;
	size_t	i;

	i = 0;
	line = 0;
	game->token = ft_memalloc(sizeof(char**) * game->token_dim[0]);
	while (i < game->token_dim[0])
	{
		get_next_line(fd, &line);
		game->token[i] = ft_strsub(line, 0, ft_strlen(line));
		ft_strdel(&line);
		i++;
	}
}

static int		ft_enough_space(t_game *game, size_t x, size_t y)
{
	if (x + game->token_dim[0] > game->dim[0])
		return (1);
	if (y + game->token_dim[1] > game->dim[1])
		return (1);
	return (0);
}

/*
** check_validity
** purpose:			This function checks if the place you want to place the piece
**					in is valid and it will return 1 if the place you wish to place
**					it in is valid. The biggest issue is dealing with the offset
**					from playing with bigger pieces. This function should also take
**					that into considertion first.
*/

static int		check_validity(t_game *game, size_t x, size_t y)
{
	size_t		size;
	size_t		c;
	size_t		i;
	size_t		w;
	char		opp;

	c = 0;
	i = 0;
	if (game->piece == 'O')
		opp = 'X';
	else
		opp = 'O';
	//if (ft_enough_space(game, x, y))
		//return (0);
	while (i + game->offset[1] < game->token_dim[0] - game->offset[3])
	{
		w = 0;
		while (w + game->offset[0] < game->token_dim[1] - game->offset[2])
		{
			if ((game->map[y + i][x + w] == game->piece) && 
					game->token[i + game->offset[1]][w + game->offset[0]] == '*')
				c++;
			if (game->map[y + i][x + w] == opp)
				c += 2;
			w++;
		}
		i++;
	}
	if (c == 1)
		return (1);
	return (0);
}

t_moves			*next_move(size_t x, size_t y)
{
	t_moves		*moves;

	moves = ft_memalloc(sizeof(moves));
	if (!moves)
		return (NULL);
	moves->play = ft_memalloc(sizeof(size_t) * 2);
	moves->next = NULL;
	moves->play[0] = y;
	moves->play[1] = x;
	return (moves);
}

static void		ft_sendplay(t_game *game, size_t x, size_t y)
{
	t_moves		*moves;
	
	if (game->moves)
	{
		moves = game->moves;
		while (moves->next != NULL)
			moves = moves->next;
		moves->next = next_move(x, y);
	}
	else
	{
		game->moves = next_move(x, y);
	}
}

/*
** play_piece
** purpose:		Finds the first valid spot to place the piece and sends a message
**				to the stdout of the position it wants to play. In the case that
**				it can't find a valid spot it will send "-1 -1" to the stdout.
*/

void			place_piece(t_game *game)
{
	size_t	x;
	size_t	y;
	size_t	played;

	y = 0;
	played = 0;
	while (y + game->token_dim[0] - (game->offset[3] + game->offset[1]) <= game->dim[0])
	{
		x = 0;
		while (x + game->token_dim[1] - (game->offset[0] + game->offset[2]) <= game->dim[1])
		{
			if (check_validity(game, x, y))
			{
				ft_sendplay(game, x, y);
				played = 1;
				//break ;
			}
			x++;
		}
		//if (played == 1)
			//break ;
		y++;
	}
	if (!played)
		ft_putendl("");
}

void		destroy_moves(t_game *game)
{
	t_moves	*moves;
	t_moves	*prev;

	moves = game->moves;
	game->moves = NULL;
	while (moves)
	{
		prev = moves;
		moves = moves->next;
		ft_memdel((void**)&prev->play);
		ft_memdel((void**)&prev);
	}
}

void		empty_map(t_game *game)
{
	size_t	itr;

	itr = 0;
	while (itr < game->dim[0])
	{
		ft_strdel(&game->map[itr]);
		itr++;
	}
	free(game->map);
	game->map = NULL;
	itr = 0;
	while (itr < game->token_dim[0])
	{
		ft_strdel(&game->token[itr]);
		itr++;
	}
	free(game->token);
	game->token = NULL;
}

int         main(int ac, char **av)
{
	int		fd;
	char	*line;
	t_game	*game;

	if (ac != 2)
    	return (1);
	line = NULL;
	if ((fd = open(av[1], O_RDONLY)) < 0)
		return (3);
	if ((game = ft_memalloc(sizeof(*game))) == NULL)
		return (4);
	get_next_line(fd, &line);
	determine_player(&line, game);
	get_map(game, fd);
	int		i = 0;

	ft_putspurple("TESTING Player number identification: ");
	ft_putsblue(ft_itoa(game->p));
	ft_putendl("");
	ft_putspurple("Testing map copying: \n");
	while (i < game->dim[0])
	{
		ft_putsgreen("Line ");
		ft_putsgreen(ft_itoa(i));
		ft_putsgreen(": \t");
		ft_putendl(game->map[i++]);
	}
	ft_putspurple("MAP CORRECTLY COPIED TO ");
	ft_putsyellow("game->");
	ft_putsblue("map");
	ft_putspurple(" STRUCT FIELD.\n");
	ft_putendl("");
	get_next_line(fd, &line);
	read_token_dim(&line, game);
	get_token(game, fd);
	i = 0;
	ft_putspurple("Testing Token copying: \n");
	while (i < game->token_dim[0])
		ft_putendl(game->token[i++]);
	ft_putspurple("TOKEN CORRECTLY COPIED TO ");
	ft_putsyellow("game->");
	ft_putsblue("token");
	ft_putspurple(" STRCUT FIELD\n");
	ft_putendl("");
	ft_putspurple("Testing the player charcater: ");
	ft_putchar(game->piece);
	ft_putchar('\n');
	calculate_offset(game);
	ft_putspurple("Testing offset left: \t");
	ft_putendl(ft_itoa(game->offset[0]));
	ft_putspurple("Testing offset top: \t");
	ft_putendl(ft_itoa(game->offset[1]));
	ft_putspurple("Testing offset right: \t");
	ft_putendl(ft_itoa(game->offset[2]));
	ft_putspurple("Testing offset bottom: \t");
	ft_putendl(ft_itoa(game->offset[3]));
	ft_putspurple("Testing output: ");
	place_piece(game);
	t_moves *moves;
	moves = game->moves;
	while (moves)
	{
		ft_putnbr(moves->play[0]);
		ft_putchar(' ');
		ft_putnbr(moves->play[1]);
		ft_putendl("");
		moves = moves->next;
	}
	destroy_moves(game);
	empty_map(game);
	if (game->moves == NULL)
		ft_putsgreen("\nManaged to free my moves\n");
	if (game->map == NULL)
		ft_putsgreen("Managed to free map\n");
	if (game->token == NULL)
		ft_putsgreen("Managed to free token\n");
	//printf("%s %s\n", *game->map, *game->token);
	return (1);
}
